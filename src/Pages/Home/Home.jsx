import React, { Fragment } from 'react';

import Header from "../../Layout/Header/Header";

function Home() {
  return (
    <Fragment>
      <Header />
      <main className="container">
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum laboriosam inventore voluptate odio beatae explicabo laborum eum vitae adipisci fugit, quidem aperiam possimus, reiciendis quis officia ipsa! Accusantium, tenetur atque! Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eveniet a beatae ex itaque non mollitia eos iure dignissimos, voluptatum consequuntur tempora, voluptatem et praesentium explicabo, accusantium totam est. Dignissimos ea quaerat ratione vero culpa nam doloremque, recusandae beatae rerum, non blanditiis molestiae, ab quas corrupti quasi reiciendis temporibus odio sint perferendis. Iste, repellendus minus? Porro dolores, quam, ad nam natus consequatur quasi harum reiciendis ipsam aspernatur eos repellendus, velit magnam pariatur! Modi corrupti reiciendis minima, facilis ipsa animi vero debitis possimus quo rem nemo, fuga, corporis earum ratione doloribus nobis. Laboriosam, ipsa illum. Doloremque, vel veritatis corporis ullam itaque accusantium totam ex aspernatur quisquam odit omnis quas debitis distinctio hic libero id perspiciatis eius consequuntur labore esse modi. Atque eligendi voluptas dolore fugit minus, sed ab, ea, perferendis consequatur maxime vitae labore vel impedit similique accusamus mollitia ducimus autem repudiandae incidunt asperiores omnis odio veritatis voluptates. Voluptas dignissimos quod aliquid debitis illo, facere doloribus laborum molestiae consequuntur atque. Unde inventore error sunt saepe dignissimos incidunt voluptatum atque! Dolor adipisci autem tenetur consectetur ipsa similique voluptas vero repellat quis, animi molestias, necessitatibus repudiandae minus voluptatibus sunt commodi, ullam reprehenderit est suscipit. Enim vitae, neque modi repellendus blanditiis et quibusdam eos excepturi?</p>
      </main>
    </Fragment>
  )
}

export default Home